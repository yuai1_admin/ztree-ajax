package com.qq.controller;

import com.alibaba.fastjson.JSONObject;
import com.qq.entity.Menu;
import com.qq.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MenuController {

    @Autowired
    private MenuService menuService;


    @RequestMapping(value = "/getListOffice",method = RequestMethod.GET)
    public String getListOffice(@RequestParam(value = "parentId",defaultValue = "00") String parentId, Model model){
        List<Menu> list = menuService.getList(parentId);
        model.addAttribute("list",list);
        return "";
    }


    @RequestMapping(value = "/getListOfficeAjax",method = RequestMethod.POST)
    @ResponseBody
    public String getListOffice(@RequestParam(value = "id",defaultValue = "1") String parentId){
        //Map<String , Object> mapJson = new HashMap<>();

        List<Map<String, Object>> mapList = new ArrayList();
        List<Menu> list = menuService.getList(parentId);
        for (int i=0; i<list.size(); i++){
            Menu e = list.get(i);
            if (parentId!=null && !parentId.equals(e.getId())){

                Map<String, Object> map = new HashMap();
                map.put("id", e.getId());
                map.put("pId", e.getParentId());
                map.put("name", e.getName());
                mapList.add(map);
            }
        }

        return JSONObject.toJSONString(mapList);
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String ztree(){
        return "index";
    }

}
