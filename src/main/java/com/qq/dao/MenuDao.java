package com.qq.dao;

import com.qq.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuDao {
    public List<Menu> getList(String id);
}
