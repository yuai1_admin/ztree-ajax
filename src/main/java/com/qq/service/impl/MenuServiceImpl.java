package com.qq.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.qq.dao.MenuDao;
import com.qq.entity.Menu;
import com.qq.service.MenuService;
import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuDao menuDao;

    @Override
    public List<Menu> getList(String id) {
        if(StringUtils.isEmpty(id)){
            return null;
        }

        List<Menu> list = menuDao.getList(id);

//        if(list != null) {
//            for (Menu sys : list) {
//                sys.setNodes(getList(sys.getId()));
//            }
//        }else{
//            return null;
//        }
        //此处可把list存到redis中

        return list;

    }
}
