package com.qq.service;

import com.qq.entity.Menu;

import java.util.List;

public interface MenuService {
    public List<Menu> getList(String id);
}
